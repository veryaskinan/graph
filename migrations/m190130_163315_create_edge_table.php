<?php

use yii\db\Migration;

/**
 * Handles the creation of table `edge`.
 */
class m190130_163315_create_edge_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $query = "
            CREATE TABLE IF NOT EXISTS edge (
                id INT AUTO_INCREMENT PRIMARY KEY,
                name VARCHAR(40) NULL,
                v1 INT NOT NULL COMMENT 'vertex 1',
                v2 INT NOT NULL COMMENT 'vertex 2',
                weight INT NOT NULL,
                CONSTRAINT edge_id_uindex UNIQUE (id)
            );
        ";

        \Yii::$app->db->createCommand($query)->execute();

        $query = "
            CREATE INDEX edge_vertex_id_id_fk ON edge (v1, v2);
        ";

        \Yii::$app->db->createCommand($query)->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('edge');
    }
}
