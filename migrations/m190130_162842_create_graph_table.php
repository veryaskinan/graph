<?php

use yii\db\Migration;

/**
 * Handles the creation of table `graph`.
 */
class m190130_162842_create_graph_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $query = "
            CREATE TABLE IF NOT EXISTS graph (
                id INT AUTO_INCREMENT PRIMARY KEY,
                name VARCHAR(40) NOT NULL,
                CONSTRAINT graph_id_uindex UNIQUE (id),
                CONSTRAINT graph_name_uindex UNIQUE (name)
            );
        ";

        \Yii::$app->db->createCommand($query)->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('graph');
    }
}
