<?php

use yii\db\Migration;

/**
 * Handles the creation of table `vertex`.
 */
class m190130_163103_create_vertex_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $query = "
            CREATE TABLE IF NOT EXISTS vertex (
                id INT AUTO_INCREMENT PRIMARY KEY,
                name VARCHAR(40) NULL,
                graph_id INT NOT NULL,
                CONSTRAINT vertex_id_uindex UNIQUE (id)
            );
        ";

        \Yii::$app->db->createCommand($query)->execute();

        $query = "
            CREATE INDEX vertex_graph_id_fk ON vertex (graph_id);
        ";

        \Yii::$app->db->createCommand($query)->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('vertex');
    }
}
