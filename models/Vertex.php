<?php

namespace app\models;


class Vertex extends  \yii\db\ActiveRecord
{
    public static function create(int $graphId, string $vertexName)
    {
        $newVertex = new static();
        $newVertex->name = $vertexName;
        $newVertex->graph_id = $graphId;

        if ($newVertex->save()) {
            return $newVertex;
        } else {
            throw new \Exception("Cannot save vertex to database");
        }
    }
}