<?php

namespace app\models;


class Edge extends  \yii\db\ActiveRecord
{
    public static function create(int $graphId, array $data)
    {
        $newEdge = new static();
        $newEdge->v1 = \app\models\Vertex::findOne(["name" => $data[0], "graph_id" => $graphId])->id;
        $newEdge->v2 = \app\models\Vertex::findOne(["name" => $data[1], "graph_id" => $graphId])->id;
        $newEdge->weight = $data[2];

        if ($newEdge->save()) {
            return $newEdge;
        } else {
            throw new \Exception("Cannot save vertex to database");
        }
    }
}