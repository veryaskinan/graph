<?php

namespace app\models;


class Graph extends  \yii\db\ActiveRecord
{
    public static function create(\stdClass $data)
    {
        $graph = static::findOne(['name' => $data->name]);
        if ($graph) {
            throw new \Exception("Graph <{$graph->name}> already exists");
        } else {
            $newGraph = new static();
            $newGraph->name = $data->name;
            if ($newGraph->save()) {
                return $newGraph;
            } else {
                throw new \Exception("Cannot save graph to database");
            }
        }
    }

    public function getVertex()
    {
        return $this->hasMany(\app\models\Vertex::className(), ['graph_id' => 'id']);
    }

    public function getEdge()
    {
        return $this
            ->hasMany(\app\models\Edge::className(), ['v1' => 'id'])
            ->viaTable('vertex', ['graph_id' => 'id'])
        ;
    }
}