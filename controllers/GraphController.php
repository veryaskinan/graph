<?php

namespace app\controllers;

use yii\web\Controller;

class GraphController extends Controller
{
    public function actionAdd()
    {
        $result = new \stdClass();

        // get application/json data from request
        $data = json_decode(file_get_contents('php://input'));

        // creating new graph
        try {
            $graph = \app\models\Graph::create($data);
        } catch (\Exception $exception) {
            $result->success = false;
            $result->error = $exception->getMessage();
            $result->errorCode = 1;
            return $this->asJson($result);
        }

        //creating vertexes and adding them to the graph
        if (is_array($data->vertex) && count($data->vertex) > 0) {
            $vertexUniqueArray = array_unique($data->vertex);
            foreach ($vertexUniqueArray as $vertex) {
                $graph->link('vertex', \app\models\Vertex::create($graph->id, $vertex));
            }
        }

        //creating edges and adding them to the graph
        if (is_array($data->edge) && count($data->edge) > 0) {
            $edgeUniqueArray = array_unique($data->edge, SORT_REGULAR);
            foreach ($edgeUniqueArray as $edge) {
                $graph->link('edge', \app\models\Edge::create($graph->id, $edge));
            }
        }

        $result->success = true;
        $result->graph = $graph;

        return $this->asJson($result);
    }

    public function actionDelete()
    {
        var_dump('delete graph');
    }

    public function actionAddVertex()
    {
        var_dump('add vertex');
    }

    public function actionDeleteVertex()
    {
        var_dump('delete vertex');
    }

    public function actionAddEdge()
    {
        var_dump('add edge');
    }

    public function actionDeleteEdge()
    {
        var_dump('add edge');
    }
}
